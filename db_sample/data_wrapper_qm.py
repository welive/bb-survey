# -*- coding: utf-8 -*-

import requests
from qm_config import WELIVE_API, DATASET, RESOURCE, TOKEN

def update_dataset(update):
    headers = {
        'content-type': 'text/plain',
        'Authorization': 'Bearer %s' % TOKEN
    }

    update_url = WELIVE_API + '/ods/%s/resource/%s/update' % (DATASET, RESOURCE)
    r = requests.post(update_url, data=update, headers=headers)

    if r.status_code != 200:
        print 'Error updating dataset. Code: %d' % r.status_code
        print r.text

def update_dataset_transaction(updates):
    headers = {
        'content-type': 'text/plain',
        'Authorization': 'Bearer %s' % TOKEN
    }

    update_url = WELIVE_API + '/ods/%s/resource/%s/update?transaction=true' % (DATASET, RESOURCE)

    update = '; '.join(updates)

    r = requests.post(update_url, data=update, headers=headers)

    if r.status_code != 200:
        print 'Error updating dataset. Code: %d' % r.status_code
        print r.text

def query_dataset(query):
    headers = {
        'content-type': 'text/plain',
        'Authorization': 'Bearer %s' % TOKEN
    }

    query_url = WELIVE_API + '/ods/%s/resource/%s/query' % (DATASET, RESOURCE)
    r = requests.post(query_url, data=query, headers=headers)

    if r.status_code != 200:
        print 'Error querying dataset: %d' % r.status_code
    else:
        return r.json()
