# -*- coding: utf-8 -*-

import sqlite3

DB_FILE = 'e5afb5f4-bfed-4976-b61a-b64647c48e10.db'

def update_dataset(update):
    conn = sqlite3.connect(DB_FILE)

    conn.execute(update)

    conn.commit()
    conn.close()

def update_dataset_transaction(updates):
    conn = sqlite3.connect(DB_FILE)

    for update in updates:
        conn.execute(update)

    conn.commit()
    conn.close()

def query_dataset(query):
    conn = sqlite3.connect(DB_FILE)
    conn.row_factory = sqlite3.Row

    count = 0
    rows = []

    for row in conn.execute(query):
        row_data = {}
        for index, key in enumerate(row.keys()):
            row_data[key] = row[index]

        rows.append(row_data)
        count += 1

    result = {}
    result['count'] = count
    result['rows'] = rows

    conn.commit()
    conn.close()

    return result
