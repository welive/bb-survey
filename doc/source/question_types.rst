.. _question-types:

Tipos de preguntas
------------------

Cuando se añade una pregunta a una encuesta es necesario indicar el tipo de la
misma para que el sea posible visualizarlas de forma correcta en los dispositivos
de los usuarios.

Desde la lista de encuestas es posible acceder a la lista de tipos de encuesta y
a las operaciones de modificación y borrado de las mismas.  Para ello debe
pulsarse sobre la opción "Tipos de preguntas" mostrada en la parte superior del
formulario de edición de preguntas.

.. image:: images/question_type_button.png

Al pulsar sobre dicha opción, se muestra la lista de tipos de preguntas
registrados actualmente. Para cada uno de los tipos de preguntas disponibles en
la aplicación, y que pueden ser usados en la creación de preguntas, se muestra
la siguiente información:

  * **Id**: identificador único del tipo de pregunta en el sistema.
  * **Tipo de pregunta**: se permiten los siguientes tipos: rangos, valores y texto libre.
  * **Usos**: número de usos totales que tiene dicho tipo de pregunta.
  * **Editar**: permite acceder a la edición de dicho tipo de pregunta.

.. _new-question-type:

Nuevo tipo de pregunta
----------------------

.. note::

  Para guardar los cambios tras la edición de una nueva preguntas es necesario
  pulsar el botón "Guardar".

El botón "Nuevo tipo" permite añadir un nuevo tipo de pregunta en la aplicación,
y que pueda ser utilizado en la definición de encuestas.

.. image:: images/new_type_button.png

Tras pulsar dicho botón, el sistema muestra el formulario para el registro de un
nuevo tipo de encuesta.

.. image:: images/new_question_type_form.png

Este formulario requiere los siguientes datos:

  * **Tipo**: que contiene un desplegable en el que es posible elegir uno de los siguientes valores (FREE_TEXT, RANGE_SET, VALUE_SET) explicados posteriormente.
  * **Valores permitidos**: indica, en el caso de elegir un valor RANGE_SET o VALUES_SET, qué valores se permiten para la pregunta.

Dependiendo el tipo de pregunta seleccionado por el usuario se requiere introducir
un valor distinto en el campo "Valores permitidos". Así, según se seleccione
para el tipo:

Para una pregunta de tipo texto libre (FREE_TEXT), no es necesario introducir
ningún valor en el campo 'Valores permitidos'.

.. image:: images/free_text.png

En el caso de una pregunta con un rango de valores determinado para la respuesta
es necesario introducir determinada información con una estructura determinada.

.. image:: images/range_set.png

El formato, como puede apreciarse en la imagen anterior, es el siguiente

.. code::

  {min: m, max: M, num: n, step: s},

donde m, M, n y s son números que indican el valor mínimo, máximo, número
de valores en la respuesta e incremento entre ellas. Por ejemplo, el siguiente
texto

.. code::

  {min: 1, max: 5, num: 5, step: 1}

que la pregunta con este tipo asociado mostrará 5 valores (num) al usuario, entre
1 (min) 5 (max) y con un incremento de 1 entre cada opción.

Cuando el usuario responda a una pregunta con este formato, podrá elegir entre
los valores (1, 2, 3, 4, 5) para responder a la pregunta.

.. _edit-question-type:

Edición de un tipo de pregunta
------------------------------

Los tipos de pregunta mostrados en la lista de tipos de preguntas, pueden ser
editados pulsando el botón correspondiente. La aplicación mostrará al usuario
el mismo formulario que el mostrado para la creación de un nuevo tipo de
pregunta (ver :ref:`new-question-type`).


Borrado de un tipo de pregunta
------------------------------

Durante la edición de un tipo de pregunta (ver :ref:`edit-question-type`) y si
este no se encuentra en uso en alguna encuesta, es posible llevar a cabo el
borrado del tipo de pregunta.

Para ello es necesario pulsar sobre el botón "Borrar" que se encuentra
disponible en el formulario de edición de tipos de preguntas.

.. image:: images/delete_question_type_button.png

.. note::

  El botón "Borrar" puede no encontrarse habilitado en el caso de que el tipo de
  pregunta que está siendo editado se encuentre en uso en alguna encuesta. En
  tal caso no es posible borrar el tipo de pregunta.
