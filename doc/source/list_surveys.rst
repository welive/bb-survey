.. _survey-list:

Listado de encuestas
--------------------

La página principal de la aplicación web muestra la lista de encuestas registradas
y permite llevar a cabo las acciones de búsqueda, edición y consulta de la
información asociada a cada uno de las encuestas.

.. image:: images/survey_list.png

Para cada encuesta contenida en el listado se muestra la siguiente información y
opciones:

	* **Id**: identificador único de la encuesta en el sistema.
	* **Título**: nombre descriptivo de la encuesta.
	* **Timestamp**: fecha de creación o modificación de la encuesta.
	* **Respuestas**: número de respuestas totales que ha tenido teniendo en cuenta todos sus usos.
	* **Editar**: haciendo click sobre el icono de edición de la encuesta, es posible editar sus características.
