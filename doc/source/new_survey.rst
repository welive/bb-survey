.. _new-survey:

Nueva encuesta
--------------

Para introducir un nueva encuesta en la base de datos de la aplicación se debe
pulsar el botón 'Nueva encuesta' que se encuentra en la página principal en la que
se muestra el listado encuestas disponibles.

.. image:: images/new_survey.png

Aparece entonces el formulario para la introducción de los datos de la nueva
encuesta que se quiere registrar.

.. image:: images/new_survey_form_1.png

El formulario solicita al usuario los siguientes campos:

 * **Título**: título que identifique a la encuesta.
 * **Descripción**: descripción general de la encuesta.

Como puede observarse en la imagen anterior, la lista de preguntas asociadas a
la encuesta se encuentra inicialmente vacía. Para añadir una pregunta a la
encuesta recién creada se debe pulsar el botón "Nueva pregunta".

.. image:: images/new_question_button.png

Una vez pulsado el botón de nueva pregunta el sistema muestra al usuario el
formulario para la introducción de una nueva pregunta.

.. image:: images/new_question_form.png

El usuario debe introducir, para la nueva pregunta, la siguiente información:

  * **Orden**: número que indica el orden de la pregunta en el formulario. Las preguntas deben ser numeradas y ordenadas por el creador de la encuesta.
  * **Pregunta**: texto de la pregunta.
  * **Tipo**: tipo de la pregunta (texto, rango, valores definidos, etc). Para más información consultar la sección :ref:`question-types`.

El proceso para añadir nuevas preguntas a una encuesta puede realizarse de la
forma indicada anteriormente hasta que se han incluido todas las necesarias.
Una vez terminada la edición de la encuesta es necesario pulsar el botón "Guardar"
del formulario para almacenar los cambios y volver a listado de encuestas disponibles.

.. image:: images/save_button.png

Para volver al listado de encuestas se debe pulsar el botón "Volver", siendo
descartados todos los cambios que no habían sido guardados en la edición de la
encuesta actual.

.. image:: images/return_button.png
