Editar encuesta
---------------

Tras pulsar el botón para la edición una encuesta ya existente, la aplicación
muestra la pantalla de edición de la encuesta, que contiene la misma información
introducida durante el proceso del registro (ver la sección :ref:`new-survey` para
más información).

.. image:: images/edit_survey.png
