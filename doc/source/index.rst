Gestor de encuestas - Manual de usuario
=======================================

En este manual se detalla el uso de la aplicación web para la gestión de encuestas.
Esta aplicación está dirigida a los administradores de los datos, mientras que
los usuarios acceden y hacen uso de los mismos a través de la aplicación móvil.

.. note::

	Para poder hacer uso de la aplicación es necesario tener una cuenta en el
	sistema de WeLive con privilegios de administrador para la aplicación.
	Si no dispone de una cuenta con privilegios de administrador, póngase en
	contacto con unai.aguilera@deusto.es.


Tabla de contenidos
===================

.. toctree::

	access
	list_surveys
	new_survey
	edit_survey
	delete_survey
	question_types
