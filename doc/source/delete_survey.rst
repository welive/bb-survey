Borrado de una encuesta
-----------------------

.. warning::

	El borrado de una encuesta es definitivo y no puede deshacerse.

Para borrar una encuesta es necesario ir a la pantalla de edición de la encuesta
(ver la sección :ref:`survey-list` para más información sobre cómo editar una
encuesta).

Durante la edición de la encuesta, la aplicación web muestra al usuario el botón
'Borrar' que permite llevar a cabo el borrado de la encuesta actual. Esto
borrará la encuesta completamente, incluidas todas las preguntas asociadas.

.. image:: images/delete_survey.png


.. note::

	Solamente es posible borrar aquellas encuestas que no tengan preguntas asociadas.
	
