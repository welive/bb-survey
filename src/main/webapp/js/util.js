function pendingChanges() {
  $("#message").removeClass("green");
  $("#message").addClass("red");
  $("#saveButton").prop("disabled", false);
  $("#message").text("Hay cambios pendientes de guardar");
}

function changesSaved() {
  $("#saveButton").prop("disabled", true);
  $("#message").removeClass("red");
  $("#message").addClass("green");
  $("#message").text("Cambios guardados correctamente");
}

function errorMessage(message) {
  $("#message").removeClass("green");
  $("#message").addClass("red");
  $("#message").text(message);
}

function checkQuestions() {
  var numQuestions = $("[id^='question-group-']").length;
  if (numQuestions == 0) {
    errorMessage("Es necesario añadir preguntas antes de guardar la encuesta.");
    $("#saveButton").prop("disabled", true);
    return false;
  }

  return true;
}

function addQuestion() {
  var numQuestions = $("[id^='question-group-']").length;
  var qNum = numQuestions + 1;

  var formGroup = $("<div/>");
  formGroup.attr("id", "question-group-" + qNum);
  formGroup.addClass("form-group");

  var orderDiv = $("<div/>");
  orderDiv.addClass("col-sm-1");
  formGroup.append(orderDiv);

  var order = $("<input/>");
  order.attr("id", "question-order-" + qNum);
  order.attr("name", "question-order-" + qNum);
  order.addClass("form-control");
  order.attr("type", "text");
  orderDiv.append(order);

  var textDiv = $("<div/>");
  textDiv.addClass("col-sm-5");
  formGroup.append(textDiv);

  var text = $("<input/>");
  text.attr("id", "question-text-" + qNum);
  text.attr("name", "question-text-" + qNum);
  text.addClass("form-control");
  text.attr("type", "text");
  textDiv.append(text);

  var selectDiv = $("<div/>");
  selectDiv.addClass("col-sm-4");
  select = parseQuestionTypes();
  select.attr("id", "question-type-" + qNum);
  select.attr("name", "question-type-" + qNum);
  selectDiv.append(select);
  formGroup.append(selectDiv);

  var buttonDiv = $("<div/>");
  buttonDiv.addClass("col-sm-2");
  formGroup.append(buttonDiv);

  var removeButton = $("<input/>");
  removeButton.attr("id", "removeQuestion-" + qNum);
  removeButton.attr("type", "button");
  removeButton.addClass("btn btn-danger pull-right");
  removeButton.attr("onclick", "removeQuestion(" + qNum + ");");
  removeButton.val("-");
  buttonDiv.append(removeButton);

  $("#questionSetForm").append(formGroup);
  pendingChanges();
}

function removeQuestion(id) {
  $("#question-group-" + id).remove();
  pendingChanges();
}

function parseQuestionTypes() {
  var qTypes = JSON.parse($("#qtypes").val());
  var select = $("<select/>");
  select.addClass("form-control");

  $.each(qTypes, function(key, value) {
      var option = $("<option/>");
      option.val(value.id);
      option.text(value.id + " " + value.type + " " + value.allowed);
      select.append(option);
  });

  return select;
}
