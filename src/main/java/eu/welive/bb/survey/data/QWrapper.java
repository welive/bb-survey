/*
Copyright 2015-2018 University of Deusto

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.bb.survey.data;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.json.JsonObject;
import javax.json.JsonValue;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.log4j.Logger;

import eu.welive.bb.survey.config.Config;
import eu.welive.bb.survey.data.qm.QMConnector;
import eu.welive.bb.survey.data.qm.QMConnectorException;
import eu.welive.bb.survey.serialization.Question;
import eu.welive.bb.survey.serialization.Question.Language;
import eu.welive.bb.survey.serialization.QuestionSet;
import eu.welive.bb.survey.serialization.QuestionType;
import eu.welive.bb.survey.serialization.Response;
import eu.welive.bb.survey.serialization.ResponseSet;

public class QWrapper {
	
	public static final String ALL_PILOTS = "All";
	
	final static Logger logger = Logger.getLogger(QWrapper.class);
	
	private final QMConnector qmConnector;
	
	private final String dataset;
	private final String resource;
	
	public static final int LATEST_QUESTIONSET = -1;
	
	public static QWrapper createQWrapper() throws QWrapperException {
    	try {
	    	final Properties p = new Properties();
			try (final InputStream in = QWrapper.class.getResourceAsStream("/conf.properties")) {
				p.load(in);
			}
			
			final String weliveApi = p.getProperty("weliveapi");
			final String dataset = p.getProperty("dataset");
			final String resource = p.getProperty("resource");
			final QMConnector qmConnector = new QMConnector(weliveApi);
			
			final QWrapper qWrapper = new QWrapper(dataset, resource, qmConnector);
			return qWrapper;
    	} catch (IOException e) {
    		throw new QWrapperException("Could not connect with questionnaire dataset." + e.getMessage());
    	}
    }
	
	public QWrapper(String dataset, String resource, QMConnector qmConnector) throws QWrapperException {
		this.dataset = dataset;
		this.resource = resource;
		this.qmConnector = qmConnector;
	}
	
	public int addResponseSet(String authHeader, ResponseSet responseSet) throws QWrapperException {
		logger.debug("Adding ResponseSet for objectId " + responseSet.getObjectId() + ", QuestionSet ID: " + responseSet.getQuestionSetId());
		
		checkResponseSet(authHeader, responseSet);
		
		try {
			
			final String insertRSTemplate = "INSERT INTO ResponseSet (id, questionset_id, objectId, lang, timestamp, userId) VALUES (null, %d, '%s', '%s', '%s', '%s')";

			final String insertResponseSet = String.format(insertRSTemplate, 
				responseSet.getQuestionSetId(), 
				StringEscapeUtils.escapeSql(responseSet.getObjectId()), 
				responseSet.getLang(),
				StringEscapeUtils.escapeSql(qmConnector.createTimestamp()), 
				StringEscapeUtils.escapeSql(responseSet.getUserId()));
					
			final List<String> inserts = new ArrayList<String>();
			inserts.add(insertResponseSet);
			
			final String insertResponseTemplate = "INSERT INTO replies (id, value, responseset_id, question_id) SELECT null, '%s', MAX(id), %d FROM ResponseSet";		
			for (final Response r : responseSet.getResponses()) {
				final String insertResponse = String.format(insertResponseTemplate, 
					StringEscapeUtils.escapeSql(r.getResponse()), r.getQuestionId());
				inserts.add(insertResponse);
			}
			
			logger.debug("Executing update: ");
			logger.debug(inserts);
			
			qmConnector.setAuthHeader(authHeader);
			final int updatedRows = qmConnector.updateDatasetTransactional(inserts, dataset, resource);
			
			return updatedRows;
		} catch (QMConnectorException e) {
			throw new QWrapperException(e);
		}
	}

	private void checkResponseSet(String authHeader, ResponseSet responseSet) throws QWrapperException {
		if (responseSet.getQuestionSetId() == -1) {
			final String msg = "ResponseSet has no 'questionSetId'";
			logger.error(msg);
    		throw new QWrapperException(msg);
    	}
    	
    	if (responseSet.getLang() == null) {
    		final String msg = "ResponseSet has no 'lang'";
    		logger.error(msg);
    		throw new QWrapperException(msg);
    	}
    	
    	if (responseSet.getObjectId() == null) {
    		final String msg = "ResponseSet has no 'objectId'";
    		logger.error(msg);
    		throw new QWrapperException(msg);
    	}
    	
    	if (responseSet.getUserId() == null) {
    		final String msg = "ResponseSet has no 'userId'";
    		logger.error(msg);
    		throw new QWrapperException(msg);
    	}
    	
    	final QuestionSet questionSet = getQuestionSet(authHeader, responseSet.getQuestionSetId(), responseSet.getLang());
    	
    	final Set<Integer> questionIds = new HashSet<Integer>();
    	for (Question question : questionSet.getQuestions()) {
    		questionIds.add(question.getId());
    	}
    	
    	for (Response response : responseSet.getResponses()) {
    		if (!questionIds.contains(response.getQuestionId())) {
    			final String msg = String.format("Question '%d' not found in QuestionSet '%d'", response.getQuestionId(), questionSet.getId());
    			logger.error(msg);
    			throw new QWrapperException(msg);
    		} else {
    			questionIds.remove(response.getQuestionId());
    		}
    	}
    	
    	if (!questionIds.isEmpty()) {
    		final String msg = String.format("Some questions of QuestionSet '%d' were not answered: %s", questionSet.getId(), questionIds);
    		logger.error(msg);
    		throw new QWrapperException(msg);
    	}
	}

	public QuestionSet getQuestionSet(String authHeader, int id, Language lang) throws QWrapperException {
		logger.debug("Obtaining QuestionSet with lang: " + lang + " id: " + id);
		
		String queryTemplate = "SELECT QuestionSet.id as questionset_id, title, description, objectId, " + 
				"QuestionSet.timestamp, Question.id as id, text, QTranslation.lang, \"order\", QuestionType.id as typeId, type, allowed, labels " +
		        "FROM QuestionSet, contains, Question, QuestionType, QTranslation " + 
		        "LEFT JOIN LTranslation ON (QuestionType.id = LTranslation.questiontype_id AND LTranslation.lang = '%s') " + 
		        "WHERE contains.questionset_id = QuestionSet.id " +
		            "AND contains.question_id = Question.id " +
		            "AND QuestionType.id = Question.type_id " +
		            "AND QTranslation.question_id = Question.id " +
		            "AND (QTranslation.lang = '%s' OR QTranslation.lang = '%s') " +
		            "AND QuestionSet.id = %d";
		 
		queryTemplate += " GROUP BY Question.id ORDER BY \"order\"";
		
		String query = "";
		query = String.format(queryTemplate, lang, lang, Config.getInstance().getDefaultLanguage(), id);
		
		logger.debug("Executing query: ");
		logger.debug(query);
		
		try {
			final QuestionSet response = processQuestionQuery(authHeader, query);
			if (response.getId() != -1) {
				return response;
			}
			else {
				final String msg = String.format("QuestionSet for id '%s' not found", id);
				logger.error(msg);
				throw new QWrapperException(msg);
			}
		} catch (QMConnectorException e) {
			logger.error(e.getMessage());
			throw new QWrapperException(e);
		}
	}
	
	public List<QuestionSet> getQuestionSets(String authHeader, Language lang) throws QWrapperException {
		logger.debug("Obtaining all QuestionSets in lang: " + lang);
		
		final String queryTemplate = "SELECT QuestionSet.id as questionset_id, title, description, " + 
				"QuestionSet.timestamp, Question.id as id, text, QTranslation.lang, \"order\", QuestionType.id as typeId, type, allowed, labels " +
		        "FROM QuestionSet, contains, Question, QuestionType, QTranslation " + 
		        "LEFT JOIN LTranslation ON (QuestionType.id = LTranslation.questiontype_id) " + 
		        "WHERE contains.questionset_id = QuestionSet.id " +
		            "AND contains.question_id = Question.id " +
		            "AND QuestionType.id = Question.type_id " +
		            "AND QTranslation.question_id = Question.id " +
		            "AND QTranslation.lang = '%s' " +
		        "GROUP BY Question.id";
		
		final String query = String.format(queryTemplate, lang);
		
		logger.debug("Executing query: ");
		logger.debug(query);
		
		final List<QuestionSet> questionSets = processQuestionSetsQuery(authHeader, query);
		return questionSets; 
	}
	
	private List<ResponseSet> getResponseSets(String authHeader, int questionSetId) throws QWrapperException {
		String queryTemplate = "SELECT ResponseSet.id as responseset_id, questionset_id, userId, objectId, lang, " +
				"ResponseSet.timestamp as responseset_timestamp, value, question_id " + 
				"FROM ResponseSet, replies, Question " + 
				"WHERE replies.responseset_id = ResponseSet.id " + 
					"AND Question.id = replies.question_id " + 		     
		            "AND ResponseSet.questionset_id = %d " +
				"ORDER BY ResponseSet.timestamp DESC";
		
		final String query = String.format(queryTemplate, questionSetId);
				
		logger.debug("Executing query: ");
		logger.debug(query);
		
		try {
			return processResponseSetsQuery(authHeader, query);
		} catch (QMConnectorException e) {
			throw new QWrapperException(e);
		}
	}
	
	public List<ResponseSet> getResponseSetsFilter(String authHeader, int questionSetId, String objectId) throws QWrapperException {
		String queryTemplate = "SELECT ResponseSet.id as responseset_id, questionset_id, userId, objectId, lang, " +
				"ResponseSet.timestamp as responseset_timestamp, value, question_id " + 
				"FROM ResponseSet, replies, Question " + 
				"WHERE replies.responseset_id = ResponseSet.id " + 
					"AND Question.id = replies.question_id " + 		     
		            "AND ResponseSet.questionset_id = %d " +
					"AND ResponseSet.objectId = '%s' " +
				"ORDER BY ResponseSet.timestamp DESC";
		
		final String query = String.format(queryTemplate, questionSetId, objectId);
				
		logger.debug("Executing query: ");
		logger.debug(query);
		
		try {
			return processResponseSetsQuery(authHeader, query);
		} catch (QMConnectorException e) {
			throw new QWrapperException(e);
		}
	}
	
	private List<ResponseSet> processResponseSetsQuery(String authHeader, String query) throws QMConnectorException {
		qmConnector.setAuthHeader(authHeader);
		final JsonObject jsonResponse = qmConnector.queryDataset(query, dataset, resource);
		
		if (jsonResponse.containsKey("count")) {
			if (jsonResponse.getInt("count") == 0) {
				return Collections.emptyList();
			}
		} else {
			throw new QMConnectorException("Invalid JSON received. 'count' field not found");
		}
		
		final List<ResponseSet> responseSets = new ArrayList<ResponseSet>();
		final Map<Integer, ResponseSet> responseSetMap = new HashMap<Integer, ResponseSet>();
		
		if (jsonResponse.containsKey("rows")) {
			for (final JsonValue rowValue : jsonResponse.getJsonArray("rows")) {
				final JsonObject rowObj = (JsonObject) rowValue;
		
				final int responseSetId = rowObj.getInt("responseset_id");
				if (!responseSetMap.containsKey(responseSetId)) {
					final ResponseSet responseSet = new ResponseSet();
					responseSetMap.put(responseSetId, responseSet);
					responseSets.add(responseSet);
				}
				
				final ResponseSet responseSet = responseSetMap.get(responseSetId);				
				responseSet.setId(rowObj.getInt("responseset_id"));
				responseSet.setQuestionSetId(rowObj.getInt("questionset_id"));
				responseSet.setObjectId(rowObj.getString("objectId"));
				responseSet.setUserId(rowObj.getString("userId"));
				responseSet.setLang(Language.valueOf(rowObj.getString("lang")));
				responseSet.setTimestamp(rowObj.getString("responseset_timestamp"));
				
				final Response response = new Response();
				response.setResponse(rowObj.getString("value"));
				response.setQuestionId(rowObj.getInt("question_id"));
				
				responseSet.addResponse(response);
			}
			
			return responseSets;
		} else {
			final String msg = "Invalid JSON received. 'rows' field not found";
			throw new QMConnectorException(msg);
		}
	}
	
	private List<QuestionSet> processQuestionSetsQuery(String authHeader, final String query) throws QWrapperException {
		try {
			qmConnector.setAuthHeader(authHeader);
			final JsonObject response = qmConnector.queryDataset(query, dataset, resource);
			
			if (response.containsKey("count")) {
				if (response.getInt("count") == 0) {
					return Collections.emptyList();
				}
			} else {
				throw new QWrapperException("Invalid JSON received. 'count' field not found");
			}
			
			if (response.containsKey("rows")) {
				final Map<Integer, QuestionSet> questionSets = new HashMap<Integer, QuestionSet>();
				
				for (final JsonValue rowValue : response.getJsonArray("rows")) {
					final JsonObject rowObj = (JsonObject) rowValue;
					
					final int questionSetId = rowObj.getInt("questionset_id");
					if (!questionSets.containsKey(questionSetId)) {
						final QuestionSet questionSet = new QuestionSet();
						
						questionSet.setId(rowObj.getInt("questionset_id"));
						questionSet.setTitle(rowObj.getString("title"));
						questionSet.setDescription(rowObj.getString("description"));
						questionSet.setTimestamp(rowObj.getString("timestamp"));
					
						questionSet.setNumResponses(getNumResponses(authHeader, questionSet.getId()));
						questionSets.put(questionSetId, questionSet);
					}
					
					final QuestionSet questionSet = questionSets.get(questionSetId);
					
					final Question question = new Question();
					question.setId(rowObj.getInt("id"));
					question.setLang(Language.valueOf(rowObj.getString("lang")));
					question.setOrder(rowObj.getInt("order"));
					question.setText(rowObj.getString("text"));
					question.setTypeId(rowObj.getInt("typeId"));
					question.setType(Question.Type.valueOf(rowObj.getString("type")));
					question.setAllowedResponse(rowObj.getString("allowed"));
					
					if (rowObj.containsKey("labels")) {
						question.setResponseLabels(rowObj.getString("labels"));
					}
					
					questionSet.addQuestion(question);
				}
				
				return new ArrayList<QuestionSet>(questionSets.values());
			} else {
				throw new QMConnectorException("Invalid JSON received. 'rows' field not found");
			}
		} catch (QMConnectorException e) {
			logger.error(e.getMessage());
			throw new QWrapperException(e);
		}
	}

	private QuestionSet processQuestionQuery(String authHeader, String query) throws QMConnectorException {
		qmConnector.setAuthHeader(authHeader);
		final JsonObject response = qmConnector.queryDataset(query, dataset, resource);
		
		final QuestionSet questionSet = new QuestionSet();
		
		if (response.containsKey("count")) {
			if (response.getInt("count") == 0) {
				return questionSet;
			}
		} else {
			throw new QMConnectorException("Invalid JSON received. 'count' field not found");
		}
		
		if (response.containsKey("rows")) {
			for (final JsonValue rowValue : response.getJsonArray("rows")) {
				final JsonObject rowObj = (JsonObject) rowValue;
				
				questionSet.setId(rowObj.getInt("questionset_id"));
				questionSet.setTitle(rowObj.getString("title"));
				questionSet.setDescription(rowObj.getString("description"));
				questionSet.setTimestamp(rowObj.getString("timestamp"));
				
				final Question question = new Question();
				question.setId(rowObj.getInt("id"));
				question.setLang(Language.valueOf(rowObj.getString("lang")));
				question.setOrder(rowObj.getInt("order"));
				question.setTypeId(rowObj.getInt("typeId"));
				question.setText(rowObj.getString("text"));
				question.setType(Question.Type.valueOf(rowObj.getString("type")));
				question.setAllowedResponse(rowObj.getString("allowed"));
				
				if (rowObj.containsKey("labels")) {
					question.setResponseLabels(rowObj.getString("labels"));
				}
				
				questionSet.addQuestion(question);
			}
			
			return questionSet;
		} else {
			throw new QMConnectorException("Invalid JSON received. 'rows' field not found");
		}
	}
	
	public List<QuestionType> getQuestionTypes(String authHeader) throws QWrapperException {
		final String query = "SELECT id, type, allowed FROM QuestionType";
		
		logger.debug("Executing query: ");
		logger.debug(query);
		
		try {
			final List<QuestionType> questionTypes = processQuestionTypes(authHeader, query);
			return questionTypes;
		} catch (QMConnectorException e) {
			logger.error(e.getMessage());
			throw new QWrapperException(e);
		}
	}
	
	public int getNumQUsingQType(String authHeader, int id) throws QWrapperException {
		final String queryTemplate = "SELECT count(*) as num FROM Question WHERE type_id=%s";
		final String query = String.format(queryTemplate, id);
		
		try {
			final JsonObject jsonResponse = qmConnector.queryDataset(query, dataset, resource);
			return jsonResponse.getJsonArray("rows").getJsonObject(0).getInt("num");
		} catch (QMConnectorException e) {
			logger.error(e.getMessage());
			throw new QWrapperException(e);
		}
	}
	
	public QuestionType getQuestionType(String authHeader, int id) throws QWrapperException {
		final String queryTemplate = "SELECT id, type, allowed FROM QuestionType WHERE id=%d";
		final String query = String.format(queryTemplate, id);
		
		logger.debug("Executing query: ");
		logger.debug(query);
		
		try {
			qmConnector.setAuthHeader(authHeader);
			final List<QuestionType> questionTypes = processQuestionTypes(authHeader, query);
			return questionTypes.get(0);
		} catch (QMConnectorException e) {
			logger.error(e.getMessage());
			throw new QWrapperException(e);
		}
	}
	
	private List<QuestionType> processQuestionTypes(String authHeader, String query) throws QMConnectorException{
		qmConnector.setAuthHeader(authHeader);
		final JsonObject response = qmConnector.queryDataset(query, dataset, resource);
		
		final List<QuestionType> questionTypes = new ArrayList<QuestionType>();
		
		if (response.containsKey("count")) {
			if (response.getInt("count") == 0) {
				return questionTypes;
			}
		} else {
			throw new QMConnectorException("Invalid JSON received. 'count' field not found");
		}
		
		if (response.containsKey("rows")) {
			for (final JsonValue rowValue : response.getJsonArray("rows")) {
				final JsonObject rowObj = (JsonObject) rowValue;
				
				final QuestionType questionType = new QuestionType();
				questionType.setId(rowObj.getInt("id"));
				questionType.setType(Question.Type.valueOf(rowObj.getString("type")));
				questionType.setAllowed(rowObj.getString("allowed"));
				
				questionTypes.add(questionType);
			}
			
			return questionTypes;
		} else {
			throw new QMConnectorException("Invalid JSON received. 'rows' field not found");
		}
	}
	
	public void updateQuestionType(String authHeader, QuestionType qType) throws QWrapperException {
		final String updateTemplate = "UPDATE QuestionType SET type='%s', allowed='%s' WHERE id = %d";
		final String update = String.format(updateTemplate, qType.getType(), qType.getAllowed(), qType.getId());
		
		logger.debug("Executing update: ");
		logger.debug(update);
		
		try {
			qmConnector.setAuthHeader(authHeader);
			qmConnector.updateDataset(update, dataset, resource);
		} catch (QMConnectorException e) {
			throw new QWrapperException(e);
		}
	}
	
	public void deleteQuestionType(String authHeader, int id) throws QWrapperException {
		final String deleteTemplate = "DELETE FROM QuestionType WHERE id = %d";
		final String delete = String.format(deleteTemplate, id);
		
		logger.debug("Executing delete: ");
		logger.debug(delete);
		
		try {
			qmConnector.setAuthHeader(authHeader);
			qmConnector.updateDataset(delete, dataset, resource);
		} catch (QMConnectorException e) {
			throw new QWrapperException(e);
		}
	}
	
	public void deleteQuestionSet(String authHeader, int questionSetId) throws QWrapperException {
		final QuestionSet questionSet = getQuestionSet(authHeader, questionSetId, Config.getInstance().getDefaultLanguage());
	
		final List<String> deletes = new ArrayList<String>();
		for (Question question : questionSet.getQuestions()) {
			deletes.addAll(createRemoveQuestion(question, questionSetId));
		}
		
		final String deleteQSTemplate = "DELETE FROM QuestionSet WHERE id=%d";
		final String delete = String.format(deleteQSTemplate, questionSetId);
		deletes.add(delete);
		
		logger.debug("Executing deletes: ");
		logger.debug(deletes);
		
		try {
			qmConnector.setAuthHeader(authHeader);
			qmConnector.updateDatasetTransactional(deletes, dataset, resource);
		} catch (QMConnectorException e) {
			throw new QWrapperException(e);
		}
	}

	public void saveQuestionType(String authHeader, QuestionType qType) throws QWrapperException {
		final String insertTemplate = "INSERT INTO QuestionType (id, type, allowed) VALUES (null, '%s', '%s')";
		final String insert = String.format(insertTemplate, qType.getType(), qType.getAllowed());
		
		logger.debug("Executing insert: ");
		logger.debug(insert);
		
		try {
			qmConnector.setAuthHeader(authHeader);
			qmConnector.updateDataset(insert, dataset, resource);
		} catch (QMConnectorException e) {
			throw new QWrapperException(e);
		}
	}
	
	private boolean containsQuestionId(Collection<QuestionData> questionsData, int id) {
		for (QuestionData questionData : questionsData) {
			if (questionData.getId() == id) {
				return true;
			}
		}
		
		return false;
	}
	
	private boolean containsQuestionId(List<Question> questions, int id) {
		for (Question question : questions) {
			if (question.getId() == id) {
				return true;
			}
		}
		
		return false;
	}
	
	private List<Question> getRemovedQuestions(Collection<QuestionData> questionsData, List<Question> questions) {
		final List<Question> removedQuestions = new ArrayList<Question>();
		
		for (Question question : questions) {
			if (!containsQuestionId(questionsData, question.getId())) {
				removedQuestions.add(question);
			}
		}
		
		return removedQuestions;
	}
	
	private List<QuestionData> getUpdatedQuestions(Collection<QuestionData> questionsData, List<Question> questions) {
		final List<QuestionData> updatedQuestions = new ArrayList<QuestionData>();
		
		for (QuestionData questionData : questionsData) {
			if (containsQuestionId(questions, questionData.getId())) {
				updatedQuestions.add(questionData);
			}
		}
		
		return updatedQuestions;
	}
	
	private List<QuestionData> getNewQuestions(Collection<QuestionData> questionsData, List<Question> questions) {
		final List<QuestionData> newQuestions = new ArrayList<QuestionData>();
		
		for (QuestionData questionData : questionsData) {
			if (!containsQuestionId(questions, questionData.getId())) {
				newQuestions.add(questionData);
			}
		}
		
		return newQuestions;
	}

	public void updateQuestionSet(String authHeader, QuestionSet questionSet, Collection<QuestionData> questionsData, Language lang) throws QWrapperException {
		final List<String> updates = new ArrayList<String>();
		
		final String updateQSTemplate = "UPDATE QuestionSet SET title='%s', description='%s', timestamp='%s' WHERE id=%d";
		final String updateQS = String.format(updateQSTemplate, questionSet.getTitle(), questionSet.getDescription(), getTimestamp(), questionSet.getId());
		updates.add(updateQS);
		
		final List<Question> removedQuestions = getRemovedQuestions(questionsData, questionSet.getQuestions());
		processRemovedQuestions(questionSet, updates, removedQuestions);
		
		final List<QuestionData> updatedQuestions = getUpdatedQuestions(questionsData, questionSet.getQuestions());
		processUpdatedQuestions(updates, updatedQuestions, lang);
		
		final List<QuestionData> newQuestions = getNewQuestions(questionsData, questionSet.getQuestions());
		processNewQuestions(questionSet, lang, updates, newQuestions);
		
		logger.debug("Executing updates: ");
		logger.debug(updates);
		
		try {
			qmConnector.setAuthHeader(authHeader);
			qmConnector.updateDatasetTransactional(updates, dataset, resource);
		} catch (QMConnectorException e) {
			throw new QWrapperException(e);
		}
	}

	private void processNewQuestions(QuestionSet questionSet, Language lang, final List<String> updates,
			final List<QuestionData> newQuestions) {
		final String insertQuestionTemplate = "INSERT INTO Question (id, 'order', type_id) VALUES (null, %d, %d)";
		final String insertContainsTemplate = "INSERT INTO contains (id, questionset_id, question_id) SELECT null, %d, MAX(Question.id) FROM Question";
		final String insertQTranslationTemplate = "INSERT INTO QTranslation (id, text, lang, question_id) SELECT null, '%s', '%s', MAX(id) FROM Question";
		for (QuestionData questionData : newQuestions) {
			final String insertQuestion = String.format(insertQuestionTemplate, questionData.getOrder(), questionData.getTypeId());
			updates.add(insertQuestion);
			
			final String insertContains = String.format(insertContainsTemplate, questionSet.getId());  
			updates.add(insertContains);
		
			final String insertQTranslation = String.format(insertQTranslationTemplate, questionData.getText(), lang);
			updates.add(insertQTranslation);
		}
	}

	private void processUpdatedQuestions(final List<String> updates, final List<QuestionData> updatedQuestions, Language lang) {
		final String updateQuestionTemplate = "UPDATE Question SET 'order'=%d, type_id=%d WHERE id =%d";
		final String updateQTranslationTemplate = "UPDATE QTranslation SET text='%s' WHERE question_id=%d AND lang='%s'";
		for (QuestionData questionData : updatedQuestions) {
			final String updateQuestion = String.format(updateQuestionTemplate, questionData.getOrder(), questionData.getTypeId(), questionData.getId());
			updates.add(updateQuestion);
			
			final String updateQTranslation = String.format(updateQTranslationTemplate, questionData.getText(), questionData.getId(), lang);
			updates.add(updateQTranslation);
		}
	}
	
	private List<String> createRemoveQuestion(Question question, int questionSetId) {
		final String deleteContainsTemplate = "DELETE FROM contains WHERE questionset_id=%d AND question_id=%d";
		final String deleteQuestionTemplate = "DELETE FROM Question WHERE id=%d";
		final String deleteQTranslationTemplate = "DELETE FROM QTranslation WHERE question_id=%d";
		
		final List<String> updates = new ArrayList<String>();
		final String deleteContains = String.format(deleteContainsTemplate, questionSetId, question.getId());
		updates.add(deleteContains);
		
		final String deleteQuestion = String.format(deleteQuestionTemplate, question.getId());
		updates.add(deleteQuestion);
		
		final String deleteQTranslation = String.format(deleteQTranslationTemplate, question.getId());
		updates.add(deleteQTranslation);
		
		return updates;
	}

	private void processRemovedQuestions(QuestionSet questionSet, final List<String> updates,
			final List<Question> removedQuestions) {
		for (Question question : removedQuestions) {	
			final List<String> removalls = createRemoveQuestion(question, questionSet.getId());
			updates.addAll(removalls);
		}
	}
	
	public void saveQuestionSet(String authHeader, QuestionSet questionSet, Collection<QuestionData> questionsData, Language lang) throws QWrapperException {
		final List<String> inserts = new ArrayList<String>();
		
		final String insertQSTemplate = "INSERT INTO QuestionSet (id, title, description, timestamp) VALUES (null, '%s', '%s', '%s')"; 
		final String insertQS = String.format(insertQSTemplate, questionSet.getTitle(), questionSet.getDescription(), getTimestamp());
		inserts.add(insertQS);
		
		final String insertQuestionTemplate = "INSERT INTO Question (id, 'order', type_id) VALUES (null, %d, %d)";
		final String insertContains = "INSERT INTO contains (id, questionset_id, question_id) SELECT null, MAX(QuestionSet.id), MAX(Question.id) FROM QuestionSet, Question";
		final String insertQTranslationTemplate = "INSERT INTO QTranslation (id, text, lang, question_id) SELECT null, '%s', '%s', MAX(id) FROM Question";
		for (QuestionData questionData : questionsData) {
			final String insertQuestion = String.format(insertQuestionTemplate, questionData.getOrder(), questionData.getTypeId());
			inserts.add(insertQuestion);
			inserts.add(insertContains);
		
			final String insertQTranslation = String.format(insertQTranslationTemplate, questionData.getText(), lang);
			inserts.add(insertQTranslation);
		}
		
		logger.debug("Executing inserts: ");
		logger.debug(inserts);
		
		try {
			qmConnector.setAuthHeader(authHeader);
			qmConnector.updateDatasetTransactional(inserts, dataset, resource);
		} catch (QMConnectorException e) {
			throw new QWrapperException(e);
		}
	}
	
	private String getTimestamp() {
		return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
	}
	
	public int getNumResponses(String authHeader, int id) throws QWrapperException {
		final List<ResponseSet> responseSets = getResponseSets(authHeader, id);
		return responseSets.size();
	}
	
	public String getCSV(String authHeader, int id) throws QWrapperException {
		try {
			final QuestionSet questionSet = getQuestionSet(authHeader, id, Config.getInstance().getDefaultLanguage());
			final List<ResponseSet> responses = getResponseSets(authHeader, id);
			return createCSV(questionSet, responses);
		} catch (IOException e) {
			throw new QWrapperException(e.getMessage());
		}
	}
	
	public String getCSV(String authHeader, int id, String objectId) throws QWrapperException {
		try {
			final QuestionSet questionSet = getQuestionSet(authHeader, id, Config.getInstance().getDefaultLanguage());
			final List<ResponseSet> responses = getResponseSetsFilter(authHeader, id, objectId);
			return createCSV(questionSet, responses);
		} catch (IOException e) {
			throw new QWrapperException(e.getMessage());
		}
	}
	
	private String createCSV(QuestionSet questionSet, List<ResponseSet> responses) throws IOException {
    	final StringWriter out = new StringWriter();
   	
    	final String[] headers = getHeaders(questionSet);
		try (final CSVPrinter printer = CSVFormat.DEFAULT.withQuote('|').withHeader(headers).print(out)) {
			
			for (final ResponseSet responseSet : responses) {
				printer.printRecord(getResponse(responseSet, questionSet));
			}
			
			return out.toString();
		}
    }
	
    private String[] getHeaders(QuestionSet questionSet) {
    	final List<String> headers = new ArrayList<String>();
    	
    	headers.add("lang");
    	headers.add("timestamp");
    	
    	for (final Question question : questionSet.getQuestions()) {
    		headers.add(question.getText());
    	}
    	
    	return headers.toArray(new String[headers.size()]);
    } 
    
    private Map<Integer, String> getResponseMap(ResponseSet responseSet) {
    	final Map<Integer, String> responseMap = new HashMap<Integer, String>();
    	
    	for (final eu.welive.bb.survey.serialization.Response response : responseSet.getResponses()) {
    		responseMap.put(response.getQuestionId(), response.getResponse());
    	}
    	
    	return responseMap;
    }
    
    private Object[] getResponse(ResponseSet responseSet, QuestionSet questionSet) {
    	final List<String> responseList = new ArrayList<String>();
    	
    	responseList.add(responseSet.getLang().toString());
    	responseList.add(responseSet.getTimestamp());
    	
    	for (final Question question : questionSet.getQuestions()) {
    		final Map<Integer, String> responseMap = getResponseMap(responseSet);
    		responseList.add(responseMap.get(question.getId()));
    	}
    	
    	return responseList.toArray(new Object[responseList.size()]);
    }

	public List<QuestionSet> getQuestionSets(String authHeader) throws QWrapperException {
		final List<QuestionSet> questionSets = new ArrayList<QuestionSet>();
		
		final String query = "SELECT id, title, description, objectId, timestamp FROM QuestionSet";
		
		try {
			qmConnector.setAuthHeader(authHeader);
			final JsonObject response = qmConnector.queryDataset(query, dataset, resource);
			
			if (response.containsKey("count")) {
				if (response.getInt("count") == 0) {
					return Collections.emptyList();
				}
			} else {
				throw new QWrapperException("Invalid JSON received. 'count' field not found");
			}
			
			if (response.containsKey("rows")) {
				for (final JsonValue rowValue : response.getJsonArray("rows")) {
					final JsonObject rowObj = (JsonObject) rowValue;
					
					final QuestionSet questionSet = new QuestionSet();
					questionSet.setId(rowObj.getInt("id"));
					questionSet.setTitle(rowObj.getString("title"));
					questionSet.setDescription(rowObj.getString("description"));
					questionSet.setTimestamp(rowObj.getString("timestamp"));
					
					questionSets.add(questionSet);
				}
				
				return questionSets;
			} else {
				throw new QWrapperException("Invalid JSON received. 'rows' field not found");
			}
		} catch (QMConnectorException e) {
			throw new QWrapperException(e);
		}
	}
	
	public boolean hasResponse(String authHeader, int questionSetId, String objectId, String userId) throws QWrapperException {
		final String queryTemplate = "SELECT COUNT(id) as count FROM ResponseSet WHERE questionset_id=%d AND objectId='%s' AND userId='%s'";
		final String query = String.format(queryTemplate, 
			questionSetId,
			StringEscapeUtils.escapeSql(objectId),
			StringEscapeUtils.escapeSql(userId));
		
		logger.debug("Executing query: ");
		logger.debug(query);
		
		try {
			qmConnector.setAuthHeader(authHeader);
			final JsonObject response = qmConnector.queryDataset(query, dataset, resource);
			
			if (response.containsKey("rows")) {
				final JsonObject row = response.getJsonArray("rows").getJsonObject(0);
				if (row.containsKey("count")) {
					final int count = row.getInt("count");
					return count > 0;
				} else {
					throw new QWrapperException("Invalid JSON received. 'count' field not found");
				}
			} else {
				throw new QWrapperException("Invalid JSON received. 'rows' field not found");
			}
		} catch (QMConnectorException e) {
			throw new QWrapperException(e);
		}
	}
}
