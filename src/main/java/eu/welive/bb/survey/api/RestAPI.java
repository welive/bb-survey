/*
Copyright 2015-2018 University of Deusto

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.bb.survey.api;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import eu.welive.bb.survey.auth.aac.AACManager;
import eu.welive.bb.survey.auth.aac.BasicProfile;
import eu.welive.bb.survey.data.QWrapper;
import eu.welive.bb.survey.data.QWrapperException;
import eu.welive.bb.survey.exception.QuestionnaireException;
import eu.welive.bb.survey.serialization.MessageResponse;
import eu.welive.bb.survey.serialization.NumResponses;
import eu.welive.bb.survey.serialization.Question.Language;
import eu.welive.bb.survey.serialization.QuestionSet;
import eu.welive.bb.survey.serialization.ResponseSet;
import eu.welive.bb.survey.serialization.survey.SurveySet;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@Path("survey")
@Api(value = "survey")
public class RestAPI {

	private static final String APPLICATION_JSON_UTF8 = MediaType.APPLICATION_JSON + "; charset=utf-8";
	
    @GET
    @Path("/{id}")
    @Produces(APPLICATION_JSON_UTF8) 
    @ApiOperation(value = "Obtains the specified survey in the specified language", 
    	response = QuestionSet.class)
    public QuestionSet getQuestionSet(@HeaderParam("Authorization") @DefaultValue("") String authHeader,
    		@ApiParam(value = "Retrieves and specific survey")
    		@PathParam("id") int id,
    		@ApiParam(value = "The language of the retrieved questions (EN, ES, IT, FI, SR)")
    		@QueryParam("lang") @DefaultValue("ES") Language lang) throws QuestionnaireException {
        
    	try {
	    	final QWrapper qWrapper = QWrapper.createQWrapper();
			final QuestionSet questionSet = qWrapper.getQuestionSet(authHeader, id, lang);
    		return questionSet;
    	} catch (QWrapperException e) {
    		throw new QuestionnaireException(e);
		}
    }
    
    @POST
    @Path("/{id}/response")
    @Produces(APPLICATION_JSON_UTF8) 
    @Consumes(APPLICATION_JSON_UTF8) 
    @ApiOperation(value = "Adds a new set of responses", 
		response = MessageResponse.class)
    public MessageResponse addResponseSet(@HeaderParam("Authorization") @DefaultValue("") String authHeader,
    		@PathParam("id") int id,
    		@ApiParam(value = "The response set to add", required = true)
    		ResponseSet responseSet) throws QuestionnaireException { 
    	
    	responseSet.setQuestionSetId(id);
    	
    	final AACManager aacManager = new AACManager();
    	final BasicProfile basicProfile = aacManager.getBasicProfile(authHeader);
    	responseSet.setUserId(basicProfile.getUserId());
    	    	
    	try {
    		final QWrapper qWrapper = QWrapper.createQWrapper();
    		if (!qWrapper.hasResponse(authHeader, responseSet.getQuestionSetId(), responseSet.getObjectId(), basicProfile.getUserId())) {
	    		qWrapper.addResponseSet(authHeader, responseSet);
	    	
	    		final MessageResponse responseMessage = new MessageResponse();
	    		responseMessage.setMessage("Responses correctly stored");
	    		return responseMessage;
    		} else {
    			throw new QuestionnaireException(Status.BAD_REQUEST, "User already added response to questionset: " + responseSet.getQuestionSetId() + " with objectId: " + responseSet.getObjectId());
    		}
    	} catch (QWrapperException e) {
    		throw new QuestionnaireException(e);
		}
    }

    @GET
    @Path("/{id}/response/csv")
    @Produces(MediaType.TEXT_PLAIN)
    @ApiOperation(value = "Obtains all the responses for the specified survey in CSV format", 
		response = SurveySet.class)
    public Response getResponse(
    		@HeaderParam("Authorization") @DefaultValue("") String authHeader,
    		@ApiParam(value = "Retrieves the responses for an specific objectId")
    		@PathParam("id") int id) throws QuestionnaireException {
    	
    	try {
    		final QWrapper qWrapper = QWrapper.createQWrapper();
    		final String csv = qWrapper.getCSV(authHeader, id);
    		    		    		
    		return Response.ok(csv, MediaType.TEXT_PLAIN)
    				.header("content-disposition","attachment; filename = survey.csv")
    				.build();
    	} catch (QWrapperException e) {
    		throw new QuestionnaireException(e);
    	}
    }
    
    @GET
    @Path("/{id}/response/{objectId}/csv")
    @Produces(MediaType.TEXT_PLAIN)
    @ApiOperation(value = "Obtains the number of responses for the specified survey filtering by objectId", 
		response = SurveySet.class)
    public Response getResponseFiltered(
    		@HeaderParam("Authorization") @DefaultValue("") String authHeader,
    		@ApiParam(value = "Retrieves the responses for an specific objectId")
    		@PathParam("id") int id,
    		@PathParam("objectId") String objectId) throws QuestionnaireException {
    	
    	try {
    		final QWrapper qWrapper = QWrapper.createQWrapper();
    		final String csv = qWrapper.getCSV(authHeader, id, objectId);
    		    		    		
    		return Response.ok(csv, MediaType.TEXT_PLAIN)
    				.header("content-disposition","attachment; filename = survey.csv")
    				.build();
    	} catch (QWrapperException e) {
    		throw new QuestionnaireException(e);
    	}
    }
    
    @GET
    @Path("/{id}/response/{objectId}/count")
    @Produces(APPLICATION_JSON_UTF8) 
    @ApiOperation(value = "Obtains the responses in CSV format for the specified survey filtering by objectId", 
		response = SurveySet.class)
    public NumResponses getNumResponseFiltered(
    		@HeaderParam("Authorization") @DefaultValue("") String authHeader,
    		@ApiParam(value = "Retrieves the responses for an specific objectId")
    		@PathParam("id") int id,
    		@PathParam("objectId") String objectId) throws QuestionnaireException {
    	
    	try {
    		final QWrapper qWrapper = QWrapper.createQWrapper();
    		final List<ResponseSet> responseSets = qWrapper.getResponseSetsFilter(authHeader, id, objectId);
			return new NumResponses(responseSets.size());
    	} catch (QWrapperException e) {
    		throw new QuestionnaireException(e);
    	}
    }
    
    @GET
    @Path("/all")
    @Produces(APPLICATION_JSON_UTF8) 
    @ApiOperation(value = "Obtains the list of all the available surveys")
    public List<QuestionSet> getQuestionSets(
    		@HeaderParam("Authorization") @DefaultValue("") String authHeader) throws QuestionnaireException {
    	
    	try {
    		final QWrapper qWrapper = QWrapper.createQWrapper();
    		return qWrapper.getQuestionSets(authHeader);
    	} catch (QWrapperException e) {
    		throw new QuestionnaireException(e);
    	}
    }
        
}