/*
Copyright 2015-2018 University of Deusto

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.bb.survey.web.app;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.thymeleaf.ITemplateEngine;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.templatemode.TemplateMode;
import org.thymeleaf.templateresolver.ServletContextTemplateResolver;

import eu.welive.bb.survey.web.controller.IController;
import eu.welive.bb.survey.web.controller.general.HomeController;
import eu.welive.bb.survey.web.controller.general.LoginController;
import eu.welive.bb.survey.web.controller.general.LogoutController;
import eu.welive.bb.survey.web.controller.general.RootRedirectController;
import eu.welive.bb.survey.web.controller.qtype.DeleteQuestionType;
import eu.welive.bb.survey.web.controller.qtype.EditQuestionType;
import eu.welive.bb.survey.web.controller.qtype.ListQuestionTypes;
import eu.welive.bb.survey.web.controller.qtype.NewQuestionType;
import eu.welive.bb.survey.web.controller.qtype.SaveQuestionType;
import eu.welive.bb.survey.web.controller.questionset.CSVDownload;
import eu.welive.bb.survey.web.controller.questionset.DeleteQuestionSet;
import eu.welive.bb.survey.web.controller.questionset.EditQuestionSet;
import eu.welive.bb.survey.web.controller.questionset.ListQuestionSets;
import eu.welive.bb.survey.web.controller.questionset.NewQuestionSet;
import eu.welive.bb.survey.web.controller.questionset.SaveQuestionSet;
  
public class Application {

	private TemplateEngine templateEngine;
	private Map<String, IController> controllersByURL;

	public Application(final ServletContext servletContext) {
		ServletContextTemplateResolver templateResolver = new ServletContextTemplateResolver(servletContext);

		templateResolver.setTemplateMode(TemplateMode.HTML);
		templateResolver.setPrefix("templates/");
		templateResolver.setSuffix(".html");
		templateResolver.setCacheTTLMs(Long.valueOf(3600000L));

		templateResolver.setCacheable(true);

		this.templateEngine = new TemplateEngine();
		this.templateEngine.setTemplateResolver(templateResolver);

		this.controllersByURL = new HashMap<String, IController>();
		this.controllersByURL.put("/", new RootRedirectController());
		this.controllersByURL.put("/index", new HomeController());
		this.controllersByURL.put("/login", new LoginController());
		this.controllersByURL.put("/logout", new LogoutController());
		
		this.controllersByURL.put("/listQuestionTypes", new ListQuestionTypes());
		this.controllersByURL.put("/editQuestionType", new EditQuestionType());
		this.controllersByURL.put("/saveQuestionType", new SaveQuestionType());
		this.controllersByURL.put("/newQuestionType", new NewQuestionType());
		this.controllersByURL.put("/deleteQuestionType", new DeleteQuestionType());
		
		this.controllersByURL.put("/listQuestionSets", new ListQuestionSets());
		this.controllersByURL.put("/saveQuestionSet", new SaveQuestionSet());
		this.controllersByURL.put("/newQuestionSet", new NewQuestionSet());
		this.controllersByURL.put("/editQuestionSet", new EditQuestionSet());
		this.controllersByURL.put("/deleteQuestionSet", new DeleteQuestionSet());
		this.controllersByURL.put("/downloadCSV", new CSVDownload());
	}

	public IController resolveControllerForRequest(final HttpServletRequest request) {
		final String path = getRequestPath(request);
		return this.controllersByURL.get(path);
	}

	public ITemplateEngine getTemplateEngine() {
		return this.templateEngine;
	}

	private static String getRequestPath(final HttpServletRequest request) {
		String requestURI = request.getRequestURI();
		final String contextPath = request.getContextPath();

		final int fragmentIndex = requestURI.indexOf(';');
		if (fragmentIndex != -1) {
			requestURI = requestURI.substring(0, fragmentIndex);
		}

		if (requestURI.startsWith(contextPath)) {
			return requestURI.substring(contextPath.length());
		}
		return requestURI;
	}
}