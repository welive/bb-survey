/*
Copyright 2015-2018 University of Deusto

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.bb.survey.web.controller.questionset;

import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.thymeleaf.ITemplateEngine;
import org.thymeleaf.context.WebContext;

import com.fasterxml.jackson.databind.ObjectMapper;

import eu.welive.bb.survey.data.QWrapper;
import eu.welive.bb.survey.serialization.QuestionSet;
import eu.welive.bb.survey.serialization.QuestionType;
import eu.welive.bb.survey.web.controller.CommonController;

public class NewQuestionSet extends CommonController {

	@Override
	public void processLogged(HttpServletRequest request, HttpServletResponse response, ServletContext servletContext,
			ITemplateEngine templateEngine, WebContext ctx) throws Exception {

		final QuestionSet questionSet = new QuestionSet();
		
		final QWrapper qWrapper = QWrapper.createQWrapper();
		final List<QuestionType> questionTypes = qWrapper.getQuestionTypes(getBearerToken());

		ctx.setVariable("editing", false);
		ctx.setVariable("qset", questionSet);

		final ObjectMapper mapper = new ObjectMapper();
		final String jsonTypes = mapper.writeValueAsString(questionTypes);
		ctx.setVariable("questiontypesjson", jsonTypes);
		
		templateEngine.process("editQuestionSet", ctx, response.getWriter());
	}

}
