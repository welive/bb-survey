/*
Copyright 2015-2018 University of Deusto

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.bb.survey.web.controller.general;

import java.io.StringReader;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.thymeleaf.ITemplateEngine;
import org.thymeleaf.context.WebContext;

import eu.welive.bb.survey.auth.aac.AACManager;
import eu.welive.bb.survey.auth.aac.ExtendedProfile;
import eu.welive.bb.survey.config.Config;
import eu.welive.bb.survey.web.controller.CommonController;
import eu.welive.bb.survey.web.controller.IController;

public class HomeController implements IController {
	
	public HomeController() {
		super();
	}
	
	private JsonObject getTokenInfo(String code, String redirectURL) {
		final Client client = ClientBuilder.newClient();
		final WebTarget target = client.target(Config.getInstance().getAacServiceURL()).path("/oauth/token");
		
		final Response response = target
			.queryParam("client_id", Config.getInstance().getClientId())
			.queryParam("client_secret", Config.getInstance().getClientSecret())
			.queryParam("grant_type", "authorization_code")
			.queryParam("code", code)
			.queryParam("redirect_uri", redirectURL)
			.request(MediaType.APPLICATION_JSON)
		.get();
		
		final JsonReader jsonReader = Json.createReader(new StringReader(response.readEntity(String.class)));
		final JsonObject jsonResponse = jsonReader.readObject();
		jsonReader.close();
		
		return jsonResponse;
	}

	public void process(final HttpServletRequest request, final HttpServletResponse response,
			final ServletContext servletContext, final ITemplateEngine templateEngine) throws Exception {
		
		if (request.getParameter("code") != null) {
			final JsonObject tokenInfo = getTokenInfo(request.getParameter("code"), Config.getInstance().getHomeURL());
			final String token = tokenInfo.getString("access_token"); 
			
			request.getSession(true).setAttribute(CommonController.TOKEN, token);
			
			final AACManager aacManager = new AACManager();
			final ExtendedProfile profile = aacManager.getExtendedProfile("Bearer " + token);
			
			request.getSession(true).setAttribute(CommonController.USER_ID, profile.getBasicProfile().getUserId());
			request.getSession(true).setAttribute(CommonController.FULL_NAME, profile.getBasicProfile().getName() + " " + profile.getBasicProfile().getSurname());
			request.getSession(true).setAttribute(CommonController.EMAIL, profile.getAccountProfile().getEmail());
		}
		
		if (request.getSession(true).getAttribute(CommonController.TOKEN) != null) {			
			response.sendRedirect("listQuestionSets");
		} else {
			WebContext ctx = new WebContext(request, response, servletContext, request.getLocale());
			ctx.setVariable("clientId", Config.getInstance().getClientId());
			ctx.setVariable("aacServiceURL", Config.getInstance().getAacServiceURL());
			
			templateEngine.process("index", ctx, response.getWriter());
		}
	}
}