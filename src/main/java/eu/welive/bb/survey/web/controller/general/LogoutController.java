/*
Copyright 2015-2018 University of Deusto

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.bb.survey.web.controller.general;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

import org.thymeleaf.ITemplateEngine;
import org.thymeleaf.context.WebContext;

import eu.welive.bb.survey.config.Config;
import eu.welive.bb.survey.web.controller.CommonController;

public class LogoutController extends CommonController {
	
	private Response revokeToken() {
		final Client client = ClientBuilder.newClient();
		final WebTarget target = client.target(Config.getInstance().getAacServiceURL()).path("/eauth/revoke/" + token);
		
		return target.request().get();
	}

	@Override
	public void processLogged(HttpServletRequest request, HttpServletResponse response, ServletContext servletContext,
			ITemplateEngine templateEngine, WebContext ctx) throws Exception {
		
		revokeToken();
		invalidateToken(request);
		response.sendRedirect(Config.getInstance().getAacServiceURL() + "/logout?service=" + Config.getInstance().getHomeURL());
	}

}
