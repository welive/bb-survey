/*
Copyright 2015-2018 University of Deusto

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.bb.survey.web.controller.questionset;

import java.io.BufferedReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONObject;
import org.thymeleaf.ITemplateEngine;
import org.thymeleaf.context.WebContext;

import eu.welive.bb.survey.config.Config;
import eu.welive.bb.survey.data.QWrapper;
import eu.welive.bb.survey.data.QWrapperException;
import eu.welive.bb.survey.data.QuestionData;
import eu.welive.bb.survey.serialization.QuestionSet;
import eu.welive.bb.survey.web.controller.CommonController;

public class SaveQuestionSet extends CommonController { 

	@Override
	public void processLogged(HttpServletRequest request, HttpServletResponse response, ServletContext servletContext,
			ITemplateEngine templateEngine, WebContext ctx) throws Exception {
		
		final StringBuffer sb = new StringBuffer();
		try (final BufferedReader reader = request.getReader()) {
			String line = null;
			while ((line = reader.readLine()) != null) {
				sb.append(line);
			}
		}
		 
		final String json = sb.toString();
		final JSONObject data = new JSONObject(json);

		final String title = data.getString("title");
		final String description = data.getString("description");
		
		if (data.has("editing") && data.getBoolean("editing")) {
			final int id = data.getInt("id");
			final Collection<QuestionData> questionsData = processQuestions(data.getJSONArray("questions"));			
			final QWrapper qWrapper = QWrapper.createQWrapper();
			
			final QuestionSet questionSet = qWrapper.getQuestionSet(getBearerToken(), id, Config.getInstance().getDefaultLanguage());
			questionSet.setTitle(title);
			questionSet.setDescription(description);
			
			qWrapper.updateQuestionSet(getBearerToken(), questionSet, questionsData, Config.getInstance().getDefaultLanguage());
		} else {
			final Collection<QuestionData> questionsData = processQuestions(data.getJSONArray("questions"));			
			final QWrapper qWrapper = QWrapper.createQWrapper();
			
			final QuestionSet questionSet = new QuestionSet();
			questionSet.setTitle(title);
			questionSet.setDescription(description);
			
			qWrapper.saveQuestionSet(getBearerToken(), questionSet, questionsData, Config.getInstance().getDefaultLanguage());
		}
		
		response.sendRedirect("listQuestionSets");
	}

	private Collection<QuestionData> processQuestions(JSONArray questions) throws QWrapperException {
		final List<QuestionData> questionList = new ArrayList<QuestionData>();
		
		for (int i = 0; i < questions.length(); i++) {
			final JSONObject question = questions.getJSONObject(i);
			final QuestionData questionData = new QuestionData();
			if (question.has("id")) {
				final int id = question.getInt("id");
				questionData.setId(id);
			} 
			questionData.setOrder(question.getInt("order"));
			questionData.setText(question.getString("text"));
			questionData.setTypeId(question.getInt("type"));
			questionList.add(questionData);
		}
		
		return questionList;
	}

}
