/*
Copyright 2015-2018 University of Deusto

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.bb.survey.web.controller.qtype;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.thymeleaf.ITemplateEngine;
import org.thymeleaf.context.WebContext;

import eu.welive.bb.survey.data.QWrapper;
import eu.welive.bb.survey.serialization.Question;
import eu.welive.bb.survey.serialization.Question.Type;
import eu.welive.bb.survey.serialization.QuestionType;
import eu.welive.bb.survey.web.controller.CommonController;

public class SaveQuestionType extends CommonController {

	@Override
	public void processLogged(HttpServletRequest request, HttpServletResponse response, ServletContext servletContext,
			ITemplateEngine templateEngine, WebContext ctx) throws Exception {
		
		final String id = request.getParameter("id");
		final String type = request.getParameter("type");
		final String allowed = request.getParameter("allowed");
		final String editing = request.getParameter("editing");
		
		if (id != null && 
			(type != null && 
				(Type.valueOf(type) != Type.FREE_TEXT && allowed != null) ||
				(Type.valueOf(type) == Type.FREE_TEXT)) && 
			editing != null) {
			final QuestionType qType = new QuestionType();
			qType.setId(Integer.parseInt(id));
			qType.setType(Question.Type.valueOf(type));
			qType.setAllowed(allowed);
			
			final QWrapper qWrapper = QWrapper.createQWrapper();
			if (Boolean.parseBoolean(editing)) {
				qWrapper.updateQuestionType(getBearerToken(), qType);
			} else {
				qWrapper.saveQuestionType(getBearerToken(), qType);
			}
		}
		
		response.sendRedirect("listQuestionTypes");
	}

}
