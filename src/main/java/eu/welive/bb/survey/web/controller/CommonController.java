/*
Copyright 2015-2018 University of Deusto

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.bb.survey.web.controller;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.thymeleaf.ITemplateEngine;
import org.thymeleaf.context.WebContext;

public abstract class CommonController implements IController {
	
	public static final String TOKEN = "token";
	public static final String USER_ID = "userId";
	public static final String FULL_NAME = "fullName";
	public static final String EMAIL = "email";
	
	protected boolean isLogged = false;
	protected String token = "";
	protected String userId = "";
	protected String fullName = "";
	protected String email = "";
	
	@Override
	public void process(HttpServletRequest request, HttpServletResponse response, ServletContext servletContext,
			ITemplateEngine templateEngine) throws Exception {
		
		if (request.getSession(true).getAttribute(TOKEN) != null) {
			token = (String) request.getSession(true).getAttribute(TOKEN);
		}
		
		if (token.isEmpty()) {
			response.sendRedirect("index");
		} else {
			isLogged = true;
			
			userId = (String) request.getSession(true).getAttribute(USER_ID);
			fullName = (String) request.getSession(true).getAttribute(FULL_NAME);
			email = (String) request.getSession(true).getAttribute(EMAIL);
			
			WebContext ctx = new WebContext(request, response, servletContext, request.getLocale());
			ctx.setVariable(TOKEN, token);
			ctx.setVariable(FULL_NAME, fullName);
			ctx.setVariable(EMAIL, email);
			processLogged(request, response, servletContext, templateEngine, ctx);
		}
	}
	
	public String getBearerToken() {
		return "Bearer " + token;
	}
	
	public abstract void processLogged(HttpServletRequest request, HttpServletResponse response, ServletContext servletContext,
			ITemplateEngine templateEngine, WebContext ctx) throws Exception;
	
	public void invalidateToken(HttpServletRequest request) {
		request.getSession(true).removeAttribute(TOKEN);
		token = "";
	}
}
