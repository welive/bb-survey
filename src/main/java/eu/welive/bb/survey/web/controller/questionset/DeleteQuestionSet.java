/*
Copyright 2015-2018 University of Deusto

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.bb.survey.web.controller.questionset;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.thymeleaf.ITemplateEngine;
import org.thymeleaf.context.WebContext;

import eu.welive.bb.survey.data.QWrapper;
import eu.welive.bb.survey.web.controller.CommonController;

public class DeleteQuestionSet extends CommonController {

	@Override
	public void processLogged(HttpServletRequest request, HttpServletResponse response, ServletContext servletContext,
			ITemplateEngine templateEngine, WebContext ctx) throws Exception {

		final String questionSetId = request.getParameter("id");
		if (questionSetId != null) {
			final QWrapper qWrapper = QWrapper.createQWrapper();
			qWrapper.deleteQuestionSet(getBearerToken(), Integer.parseInt(questionSetId));
		}
		
		response.sendRedirect("listQuestionSets");
	}

}
