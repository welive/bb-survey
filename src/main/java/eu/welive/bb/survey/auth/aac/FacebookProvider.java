/*
Copyright 2015-2018 University of Deusto

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.bb.survey.auth.aac;

import org.json.JSONObject;

public class FacebookProvider extends Provider { 
	
	public static final String ACCOUNT_TYPE = "facebook";
	public static final String EMAIL = "email";
	
	private String email = "";
	
	public FacebookProvider(JSONObject json) {
		super(json);
		
		if (json.has(EMAIL)) {
			email = json.getString(EMAIL);
		}
	}
	
	public FacebookProvider(String givenName, String surname, String email) {
		super(givenName, surname);
		this.email = email;
	}
	
	@Override
	public String getAccountType() {
		return ACCOUNT_TYPE;
	}

	@Override
	public String getEmail() {
		return email;
	}
}
